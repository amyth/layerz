# Layerz - jQuery Layered Slideshow

### About

Layerz iz a jQuery slideshow that implements multiple layered html elements in each slide. you can fully customized the element direction and index to create awesome multi-layered slides for your slide show. You can use images, videos, flash and just about any html element as a layer in your slide.

### Requirements

+ jQuery 1.4+
+ Firefox 3.0+, Safari 4.0+, Chrome 7+, IE9+, Opera 9+

### Installation

1. Include the Latest jQuery library in the `<head>` section of your html file.
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>

2. Include the `layerz.js` library in the `head` section under jQuery Library.
      <script src="js/layerz.js" type="text/javascript"></script>

3. Bind the layerz slider to any html element.
      <script type="text/javascript">
        $(document).ready(function(){
          $("#my_slider").layerz({
              'delay' : 3000
            });
        });
      </script>

### Available Options

+ `delay` (Integer, [Delay between sliding layers])
+ `defaultdir` (Left, Right, Top, Bottom, [Default Direction to Slide From, for layers that do not have a specified direction])

### Author Information

`Author Name` : `Amyth Arora`
`Author URI` : `http://www.techstricks.com`
