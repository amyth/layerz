/*////////////////////////////////////////////////////////////////////////////////////
 * Script Name: Layerz slideshow
 * Script Version: v1.0.1
 * Description: a jQuery slideshow with layerz.
 * Author Name: Amyth Arora
 * Author URI: www.techstricks.com
 * Copyright: Copyright (c) 2011 by Amyth Arora - www.techstricks.com
 * License Type: Licensed under the MIT license
 * License URI: http://www.opensource.org/licenses/mit-license.php
//////////////////////////////////////////////////////////////////////////////////*/

// This Script is absolutely free to use and distributre as it is licensed under the
// MIT License. If you have not recieved a copy of the license with this distribution,
// Please check http://www.opensource.org/licenses/mit-license.php for details.

(function($){$.fn.layerz = function(opts, callback){

  var entity = this;
  var slideList;

  // Define Default Options for Layerz
  var defaults = {
    selector : 'div',
    slideSelector: '.layerz-slide',
    activeSlide: '.active',
    slideLayerClass: 'layerz-slide-layer',
    startingSlideIndex: 0,
    fx: '',
    easing: 'easeInOutExpo',
    loader: true,
    nav: true,
    playpause: true,
    height: 250,
    width: 700,
    onSlideLoaded: function(){},
    onSlideComplete: function(){},
    onLayerzCycleComplete: function(){}
  };

  // Override Defaults with user defined options
  var opts = $.extend({}, defaults, opts);


  // Function to check if request is coming from a mobile device
  function isMobile() {
    if( navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPod/i)
      ){
        return true;
    }
  }

  // Function Load Layerz slider
  function loadLayerz(){
    slideList = createSlideList();
    // Do the Checks
    if(!isActiveClassOk()){
      makeActive(slideList.eq(opts.startingSlideIndex));
    }
    // Do the CSS stuff
    doCss();
    //create layers for each slide
    createLayers(opts.slideSelector);
  }

  // Function to add layer class to Slides Children
  function createLayers(selector){
    var allchilds = selector+' *';
    $(allchilds).addClass(opts.slideLayerClass);
  }

  // Function to Get Position of an element
  function getPosition(element){
    var position = element.offset();
    alert(position.left + ', ' + position.top);
  }

  // Function to do all the CSS stuff
  function doCss(){
    $(opts.slideSelector).css({'display':'none'});
    $(opts.activeSlide).css({'display':'block'});
  }

  // Function to check if no active class is defined
  function isActiveClassOk(){
    var check = entity.find(opts.activeSlide);
    if (check.length === 0){
      return false;
    } else {
      return true;
    }
  }

  // Function to apply active class according to the index
  function makeActive(element){
    slideList.each(function() {
      $(this).removeClass(saniElm(opts.activeSlide));
    });
    element.addClass(saniElm(opts.activeSlide));
  }

  // Function to Create a List of slides [For Indexing Purposes]
  function createSlideList(){
    var slides = entity.find(opts.slideSelector);
    return(slides);
  }

  // Function to clean up Element Names
  function saniElm(element){
    var san = element.replace('.','').replace('#','');
    return san;
  }

  loadLayerz();


}

})(jQuery);
